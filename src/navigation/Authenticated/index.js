import React from 'react';
import {
  SafeAreaView,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  createAppContainer,
} from 'react-navigation';
import {
  createDrawerNavigator,
  DrawerItems,
} from 'react-navigation-drawer';
import {
  createStackNavigator,
} from 'react-navigation-stack';
import { Button } from 'react-native-elements';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import styles from './styles';

import SimpleList from '../../components/SimpleList';
import PlacesList from '../../components/PlacesList';
import PlacesDetail from '../../components/PlacesDetail';
import LoginScreen from '../../components/LoginScreen';
import CreateItinerary from '../../components/CreateItinerary';
import CreateItinerary2 from '../../components/CreateItinerary2';
import Register from '../../components/Register';
import MailLogin from '../../components/MailLogin';
import Localize from '../../components/LocalizeExample';
import Generico from '../../components/Generico';
import ToursList from '../../components/ToursList';
import MapExample from '../../components/MapExample';



class NavigationDrawerStructure extends React.Component {
  render() {
    const { navigationProps } = this.props;
    return (
      <SafeAreaView
        forceInset={{ top: 'always', horizontal: 'never' }}
      >
        <View style={[styles.viewFlexRow]}>
          <TouchableOpacity onPress={navigationProps.toggleDrawer}>
            <FontAwesome5 name="bars" style={[styles.bars]} size={22} />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}


const LoginScreenStackNavigator = createStackNavigator({
  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: { header: null,}
  },
  Register: {
    screen: Register,
    navigationOptions: { header: null,}
  },
  MailLogin: {
    screen: MailLogin,
    navigationOptions: { header: null,}
  },
});

const GenericoStackNavigator = createStackNavigator({
  Generico: {
    screen: Generico,
    navigationOptions: ({ navigation }) => ({
      title: 'Generico',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#010a43',
        shadowOpacity: 0,
        elevation: 0,
      },
      headerTintColor: '#fff',
    }),
  },
});


const ToursListStackNavigator = createStackNavigator({
  ToursList: {
    screen: ToursList,
    navigationOptions: ({ navigation }) => ({
      title: 'ToursList',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#010a43',
        shadowOpacity: 0,
        elevation: 0,
      },
      headerTintColor: '#fff',
    }),
  },
});

const LocalizeStackNavigator = createStackNavigator({
  Localize: {
    screen: Localize,
    navigationOptions: ({ navigation }) => ({
      title: 'Localize',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#010a43',
        shadowOpacity: 0,
        elevation: 0,
      },
      headerTintColor: '#fff',
    }),
  },
});

const CreateItinerary2StackNavigator = createStackNavigator({
  CreateItinerary2: {
    screen: CreateItinerary2,
    navigationOptions: ({ navigation }) => ({
      title: 'CreateItinerary2',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#010a43',
        shadowOpacity: 0,
        elevation: 0,
      },
      headerTintColor: '#fff',
    }),
  },
});

const SimpleListStackNavigator = createStackNavigator({
  SimpleList: {
    screen: SimpleList,
    navigationOptions: ({ navigation }) => ({
      title: 'Documentos',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#010a43',
        shadowOpacity: 0,
        elevation: 0,
      },
      headerTintColor: '#fff',
    }),
  },
});


const MapExampleStackNavigator = createStackNavigator({
  MapExample: {
    screen: MapExample,
    navigationOptions: ({ navigation }) => ({
      title: 'Map Example',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#010a43',
        shadowOpacity: 0,
        elevation: 0,
      },
      headerTintColor: '#fff',
    }),
  },
});

const PlacesStackNavigator = createStackNavigator({
  PlacesList: {
    screen: PlacesList,
    navigationOptions: ({ navigation }) => ({
      title: 'Places',
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#010a43',
        shadowOpacity: 0,
        elevation: 0,
      },
      headerTintColor: '#fff',
    }),
  },
  PlacesDetail: {
    screen: PlacesDetail,
    navigationOptions: ({ navigation }) => ({
      title: 'Detail',
      headerStyle: {
        backgroundColor: '#010a43',
        shadowOpacity: 0,
        elevation: 0,
      },
      headerTintColor: '#fff',
    }),
  },
  CreateItinerary: {
    screen: CreateItinerary,
    navigationOptions: ({ navigation }) => ({
      title: 'Create itinerary',
      headerStyle: {
        backgroundColor: '#010a43',
        shadowOpacity: 0,
        elevation: 0,
      },
      headerTintColor: '#fff',
    }),
  },

});

const DrawerNavigator = createDrawerNavigator({
  Login: {
    screen: LoginScreenStackNavigator,
    navigationOptions: {
      drawerLabel: 'Login',
      drawerIcon: () => (
        <FontAwesome5 name="file" solid size={15} />
      ),
    },
  },
  Simple: {
    screen: SimpleListStackNavigator,
    navigationOptions: {
      drawerLabel: 'Documentos',
      drawerIcon: () => (
        <FontAwesome5 name="file" solid size={15} />
      ),
    },
  },


  MapExampler: {
    screen: MapExampleStackNavigator,
    navigationOptions: {
      drawerLabel: 'Map Example',
      drawerIcon: () => (
        <FontAwesome5 name="file" solid size={15} />
      ),
    },
  },
  //NO PUEDE TENER EL MISMO NOMBRE QUE EL COMPONENTE?! !??!!
  Genericus: {
    screen: GenericoStackNavigator,
    navigationOptions: {
      drawerLabel: 'Generico',
      drawerIcon: () => (
        <FontAwesome5 name="file" solid size={15} />
      ),
    },
  },
  CreateItinerary2Stack: {
    screen: CreateItinerary2StackNavigator,
    navigationOptions: {
      drawerLabel: 'CreateItinerary2',
      drawerIcon: () => (
        <FontAwesome5 name="file" solid size={15} />
      ),
    },
  },

  //NO PUEDE TENER EL MISMO NOMBRE QUE EL COMPONENTE?! !??!!
  TourListX: {
    screen: ToursListStackNavigator,
    navigationOptions: {
      drawerLabel: 'Tours List',
      drawerIcon: () => (
        <FontAwesome5 name="file" solid size={15} />
      ),
    },
  },

  Localize: {
    screen: LocalizeStackNavigator,
    navigationOptions: {
      drawerLabel: 'Localize',
      drawerIcon: () => (
        <FontAwesome5 name="file" solid size={15} />
      ),
    },
  },

  Places: {
    screen: PlacesStackNavigator,
    navigationOptions: {
      drawerLabel: 'Places',
      drawerIcon: () => (
        <FontAwesome5 name="globe-americas" solid size={15} />
      ),
    },
  },

},
{
  contentComponent: (props) => (
    <View style={styles.drawerContent}>
      <SafeAreaView forceInset={{ horizontal: 'never' }}>
        <DrawerItems {...props} />
      </SafeAreaView>
    </View>
  ),
});


const nav = createAppContainer(DrawerNavigator);

export default nav;
