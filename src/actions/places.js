import { GET_PLACES, GET_PLACES_SUCCESS } from '../constants';

export function getPlaces () {
  return {
    type: GET_PLACES,
  }
}

export function getPlacesSuccess (places) {
  return {
    type: GET_PLACES_SUCCESS,
    payload: places,
  }
}
