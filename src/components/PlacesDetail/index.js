import React from 'react';
import { ScrollView, Text, ImageBackground, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Example from '../Slider/SliderContainer.js'
import NavigationService from '../../navigation/NavigationService'

class PlacesDetail extends React.Component  {
  _onPressButton = (item) => {
    NavigationService.navigate('CreateItinerary', {item}  );
  }

  render () {
    const { title, photo, description } = this.props.navigation.state.params.item;
    return (
      <ScrollView>
        <ImageBackground source={{uri:photo}} style={{width: '100%', height: 250,
        justifyContent: 'flex-end', borderRadius: 5}}>
          <LinearGradient colors={["#ffffff00", "black"]} style={styles.linearGradient}>
            <Text style={{color:'white', fontSize: 22, fontFamily: "Raleway-Bold",
            paddingHorizontal: 16, marginBottom: 20, position: 'absolute', bottom: 0}}>{title}</Text>

            <TouchableOpacity style={styles.button} onPress={() => this._onPressButton(title)}>
            <Text style={{color: 'white'}}>Create Tour</Text>
            </TouchableOpacity>

          </LinearGradient>
        </ImageBackground>


        <Text style={{color:'black', fontSize: 16, fontFamily: "Raleway-Bold",
        paddingHorizontal: 16, paddingTop: 40, backgroundColor: 'whitesmoke'}}>Most Popular Tours</Text>
        <Example />

        <Text style={{color:'black', fontSize: 16, fontFamily: "Raleway-Bold",
        paddingHorizontal: 16, paddingTop: 20, backgroundColor: 'whitesmoke'}}>featured itineraries</Text>
        <Example />

        <Text style={{color:'black', fontSize: 16, fontFamily: "Raleway-Bold",
        paddingHorizontal: 16, marginTop: 20}}> {title}</Text>
        <Text style={{color:'grey', fontSize: 16, paddingVertical: 25, paddingHorizontal: 16, justifyContent: 'flex-end'}}>{description}</Text>
      </ScrollView>
    );
  }
}

export default PlacesDetail;

// Later on in your styles..
const styles = StyleSheet.create({
  button: {
    height: 40,
    width: 90,
    backgroundColor: 'rgba(6, 43, 116, 0.4)',
    position: 'absolute',
    bottom: 0,
    marginBottom: 16,
    right: 16,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
 },
  linearGradient: {
    flex: 1,
    justifyContent: 'space-between',
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});
