import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import NavigationService from '../../navigation/NavigationService'

// Google Sign in
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';

class LoginScreen extends React.Component  {
  _onPressButton = () => {
    NavigationService.navigate('PlacesList' );
  }
  _onPressButtonLogin = () => {
    NavigationService.navigate('MailLogin' );
  }
  _onPressButtonReg = () => {
    NavigationService.navigate('Register' );
  }
  render () {
    return (
      <View style={{flex:1, justifyContent: 'space-between',
      alignItems: 'center', backgroundColor: 'midnightblue'}}>
        <Text style={{fontSize: 34, marginBottom: 50, marginTop: 200, color: 'white',
         fontFamily: "Raleway-Bold"}}> Argentina Birds </Text>
        <View style={{alignItems: 'center'}}>
          <GoogleSigninButton
            style={{ width: 192, height: 48 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            //onPress={this._signIn}
            onPress={() => console.warn('hola')}
            //disabled={this.state.isSigninInProgress}
          />
          <TouchableOpacity style={styles.button} onPress={() => this._onPressButton()}>
          <Text style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center',
           color: 'white'}} > Continue as guest </Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.button} onPress={() => this._onPressButtonLogin()}>
          <Text style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center',
           color: 'white'}} > Login with email </Text>
          </TouchableOpacity>
           <TouchableOpacity style={styles.button} onPress={() => this._onPressButtonReg()}>
          <Text style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center',
           color: 'white'}} > Register </Text>
          </TouchableOpacity>

        </View>
      </View>


    );
  }
}

export default LoginScreen;

// Later on in your styles..
const styles = StyleSheet.create({
  button: {
    height: 40,
    width: 200,
    backgroundColor: 'rgba(6, 43, 116, 0.4)',
    alignItems: 'center',
    marginBottom: 30,
    marginTop: 30,
    alignContent: 'center',
    paddingTop: 10
 },

});
