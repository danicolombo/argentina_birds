import React from 'react';
import NavigationService from '../../navigation/NavigationService';
import * as yup from 'yup';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Formik } from 'formik';
import { Button, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import {
  ScrollView, Text, View, StyleSheet
} from 'react-native';
import { setRegister } from '../../actions/users';

const validationSchema = yup.object().shape({
  name: yup
  .string()
  .label('Name')
  .required("Required field"),
  email: yup
    .string()
    .label('Email')
    .email("Insert a valid email address")
    .required("Required field"),
  password: yup
    .string()
    .label('Password')
    .required("Required field"),
});

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.fields = {};
  }

  onSubmit = (values) => {
    const { setRegister } = this.props;
    setRegister(values)
  }

  focusNextField = (key) => {
    this.fields[key].focus();
  }

  render() {

    return (
      <Formik
        initialValues={{ email: '', password: '', id: 'frs' }}
        validationSchema={validationSchema}
        onSubmit={this.onSubmit}
      >
        {({
          values, handleChange, isValid, submitForm, errors, touched, handleBlur, handleSubmit
        }) => (
          <ScrollView>
            <Text style={styles.presentation}>Register</Text>
            <Input
              label="Name"
              value={values.name}
              onChangeText={handleChange('name')}
              onBlur={handleBlur('name')}
              placeholder="Name"
              keyboardType="default"
              labelStyle={styles.inputslabel}
              valid={touched.name && !errors.name}
              error={touched.name && errors.name}
              returnKeyType="next"
              leftIcon={(
                <Icon
                  name="user"
                  size={12}
                  color="grey"
                />
              )}
              ref={(input) => {
                this.fields.name = input;
              }}
              onSubmitEditing={() => {
                this.focusNextField('mail');
              }}
              blurOnSubmit={false}
            />
            {errors.name && touched.name ? (
                <Text style={styles.formError}>{errors.name}</Text>
            ) : null}
            <Input
            label="E-mail"
            value={values.email}
            onChangeText={handleChange('email')}
            onBlur={handleBlur('email')}
            placeholder="E-mail"
            labelStyle={styles.inputslabel}
            keyboardType="email-address"
            name= 'email'
            valid={touched.email && !errors.email}
            error={touched.email && errors.email}
            returnKeyType="next"
            autoCapitalize="none"
            leftIcon={(
              <Icon
              name="envelope"
              size={12}
              color="grey"
              />
            )}
            ref={(input) => {
              this.fields.mail = input;
            }}
            onSubmitEditing={() => {
              this.focusNextField('password');
            }}
            blurOnSubmit={false}
            />
            {errors.email && touched.email ? (
              <Text style={styles.formError}>{errors.email}</Text>
            ) : null}
            <Input
              label="Contraseña"
              value={values.password}
              onChangeText={handleChange('password')}
              placeholder="Contraseña"
              secureTextEntry
              name= 'password'
              valid={touched.password && !errors.password}
              error={touched.password && errors.password}
              labelStyle={styles.inputslabel}
              leftIcon={(
                <Icon
                  name="key"
                  size={12}
                  color="grey"
                />
              )}
              ref={(input) => {
                this.fields.password = input;
              }}
              blurOnSubmit={false}
              onSubmitEditing={() => {
                submitForm();
              }}
              returnKeyType="done"
            />
            {errors.password && touched.password ? (
                <Text style={styles.formError}>{errors.password}</Text>
            ) : null}
            <Button
              title="Enviar"
              type="outline"
              disabled={!isValid}
              onPress={handleSubmit}
              buttonStyle={styles.submitButton}
            />

          </ScrollView>
        )}
      </Formik>
    );
  }
}
const mapStateToProps = (state) => ({
  users: state.users,
});

const mapDispatchToProps = (dispatch) => ({
  setRegister: (values) => dispatch(setRegister(values)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);

// Later on in your styles..
const styles = StyleSheet.create({
  formError:{
    color: 'red',
    marginHorizontal: 16,
  },
  submitButton:{},
  inputslabel:{},
  presentation:{
    marginHorizontal: 16,
    fontSize: 20,
    fontWeight: 'bold',
    paddingVertical: 20
  },


});
