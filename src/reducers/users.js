import {GET_TOURS_SUCCESS, SET_REGISTER_SUCCESS} from '../constants'

const initialState = {
  list: [],
  tours: [],
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_REGISTER_SUCCESS:
      return {
        ...state,
        list: action.payload,
      };
      case GET_TOURS_SUCCESS:
      return {
        ...state,
        tours: action.payload,
      };
    default:
      return state;
  }
};



export default usersReducer;
