import axios from 'axios';
import Config from 'react-native-config';


export function getPlacesRequest() {
  return axios.get(`${Config.API_URL}/places`)
    .then(resp => resp.data )
    .catch((error) => {
      console.warn('ERROR', error);
    });
}

export function getToursRequest() {
  return axios.get(`${Config.API_URL}/tours`)
    .then(resp => resp.data )
    .catch((error) => {
      console.warn('ERROR api', error);
    });
}

export function setRegisterRequest(user) {
  return axios.post(`${Config.API_URL}/users`, { users: user })
    .then(resp => resp.data )
    .catch((error) => {
      console.warn('ERROR', error);
    });
}
