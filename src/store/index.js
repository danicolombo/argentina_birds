import { createStore, combineReducers, applyMiddleware } from 'redux'
import placesReducer from '../reducers/places'
import usersReducer from '../reducers/users'
import rootSaga from '../sagas';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
  places: placesReducer,
  users: usersReducer,
});

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));
//This is useful for hydrating the state of the client to match the state of a Redux application running on the server.
sagaMiddleware.run(rootSaga);
export default  store;
