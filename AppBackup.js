import React from 'react';
import Nav from './src/navigation/Authenticated';
import NavigationService from './src/navigation/NavigationService';
import SplashScreen from 'react-native-splash-screen'
import * as RNLocalize from "react-native-localize";
import {StyleSheet} from 'react-native';


class App extends React.Component {
  componentDidMount() {
    // do stuff while splash screen is shown
      // After having done stuff (such as async tasks) hide the splash screen
      SplashScreen.hide();
  }
  render() {
    return (
      <Nav ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}

export default App;

const styles = StyleSheet.create({
  title: {
    fontSize: 32,
    marginHorizontal: 16,
    marginVertical: 8,

  },
});
